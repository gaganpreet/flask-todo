#!/usr/bin/python
'''Skeleton Flask app'''

from database import db
from flask import Flask

from api.views import api_app
from ember.views import ember_app


def create_app():
    '''Initialize Flask and SQLAlchemy contexts and register blueprints'''
    app = Flask(__name__, static_folder='static', static_url_path='')
    db.init_app(app)

    app.register_blueprint(ember_app)
    app.register_blueprint(api_app)
    return app
