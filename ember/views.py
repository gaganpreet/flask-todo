from flask import Flask, Blueprint, send_file

ember_app = Blueprint('ember_app', __name__,
                    static_url_path='/ember',
                    static_folder='static',
                    url_prefix='/ember',
                    template_folder='templates')

@ember_app.route('/')
def home():
    return send_file('ember/templates/index.html', cache_timeout=600)
